import React, { useState, useEffect } from "react"
// takes two parameters:
//    A Callback function and A Dependency Array
// by default, useEffect, runs after every re-render
// cleanup function
// second parameter

const UseEffectBasics = () => {
  const [value, setValue] = useState(0)

  useEffect(() => {
    console.log("call useEffect")
    if (value > 1) {
      document.title = `New Messages(${value})`
    }
  }, [])

  console.log("render component")

  return (
    <>
      <h2>{value}</h2>
      <button className="btn" onClick={() => setValue(value + 1)}>
        click me
      </button>
    </>
  )
}

export default UseEffectBasics
